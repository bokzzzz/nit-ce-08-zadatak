class User:

    def __init__(self, name, email, ip, devices):
        self.name, self.email, self.ip, self.devices = name, email, ip, devices

    def __str__(self):
        return f"Name: {self.name}, Email: {self.email}, IP: {self.ip}, " \
               f"Devices: {self.devices}"

    def __eq__(self, other):
        return self.email == other.email

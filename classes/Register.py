from classes.User import User
import json
from json import JSONDecodeError
import re
import unittest
import json

def user_json_decoder(data):
    return User(data['name'], data['email'], data['ip'], data['devices'])


class Register:
    def __init__(self, json_files=[]):
        self.users = {}
        for file in json_files:
            self._load_json_file(file)

    def __str__(self):
        strList = []
        for user in self.users:
            strList.append(str(self.users[user]))
        return "\n".join(strList)

    def __len__(self):
        return len(self.users)

    def __add__(self, register):
        if not isinstance(register, Register):
            raise Exception("Object is not instance of Register class!")
        new_register = Register()
        new_register.users = self.users | register.users
        return new_register

    def __mul__(self, register):
        if not isinstance(register, Register):
            raise Exception("Object is not instance of Register class!")
        new_register = Register()
        new_register.users = {x: self.users[x] for x in self.users.keys() if
                              x in register.users.keys()}
        return new_register

    def __getitem__(self, item):
        if item not in self.users.keys():
            raise Exception("Key doesn't exist in Register!")
        return self.users[item]

    def __setitem__(self, key, value):
        if not isinstance(value, User):
            raise Exception("Value is not instance of User class!")
        if not self._is_valid_ip(value.ip):
            raise Exception("Ip address is not valid!")
        if key in self.users.keys():
            value.email = key
        else:
            if not self._is_valid_email(key):
                raise Exception("Email address is not valid!")
        self.users[key] = value

    def get_user_name(self, key):
        if key not in self.users.keys():
            raise Exception("Key doesn't exist in Register!")
        return self.users[key].name

    def get_user_ip(self, key):
        if key not in self.users.keys():
            raise Exception("Key doesn't exist in Register!")
        return self.users[key].ip

    def get_user_devices(self, key):
        if key not in self.users.keys():
            raise Exception("Key doesn't exist in Register!")
        return self.users[key].devices

    def set_user_name(self, key, value):
        if key not in self.users.keys():
            raise Exception("Key doesn't exist in Register!")
        self.users[key].name = value

    def set_user_ip(self, key, value):
        if key not in self.users.keys():
            raise Exception("Key doesn't exist in Register!")
        if not self._is_valid_ip(value):
            raise Exception("New value of Ip isn't correct!")
        self.users[key].ip = value

    def set_user_devices(self, key, value):
        if key not in self.users.keys():
            raise Exception("Key doesn't exist in Register!")
        if not isinstance(value, list):
            raise Exception("Value is not list!")
        if not all(isinstance(v, str) for v in value):
            raise Exception("All value of list devices need to be string!")
        self.users[key].devices = value

    def _load_json_file(self, file):
        try:
            json_file = open(file, 'r')
            data = json.load(json_file)
            for user in data:
                new_user = user_json_decoder(user)
                if self._is_valid_ip(new_user.ip) and self._is_valid_email(
                        new_user.email):
                    if new_user.email not in self.users.keys():
                        self.users[new_user.email] = new_user
                    else:
                        self._set_new_devices(new_user)
                else:
                    print(f"Email or ip not correct for user with email:"
                          f" {new_user.email}!")
            json_file.close()
        except JSONDecodeError:
            print("Json file not valid")
            exit(-1)

    def _set_new_devices(self, new_user):
        for device in new_user.devices:
            if device not in self.users[new_user.email].devices:
                self.users[new_user.email].devices.append(device)

    def _is_valid_ip(self, ip):
        ipRegex = r'\b(?:\d{1,3}\.){3}\d{1,3}\b'
        if not re.match(ipRegex, ip):
            return False
        return True

    def _is_valid_email(self, email):
        emailRegex = r'\b\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+\b'
        if not re.match(emailRegex, email):
            return False
        return True


class TestRegister(unittest.TestCase):
    def setUp(self):
        self.data = [
                {
                    "name": "Test User 1",
                    "email": "testuser1@rt-rk.com",
                    "ip": "245.125.132.6",
                    "devices": ["desktop RTRK-03", "mobile MB-546",
                                "watch W-123"]
                },
                {
                    "name": "Test User 2",
                    "email": "testuser2@rt-rk.com",
                    "ip": "2.111.255.12",
                    "devices": ["device1", "device2"]
                }
            ]
        f = open("test.json", "w")
        json.dump(self.data, f)
        f.close()
        self.register = Register(["test.json"])

    def tearDown(self):
        self.register = None

    def test_len(self):
        self.assertEqual(len(self.register), 2)

    def test_get_item(self):
        self.assertEqual(self.register['testuser1@rt-rk.com'],
                         User(**self.data[0]))

    def test_get_item_key_not_found_exception(self):
        self.assertRaises(Exception, self.register.__getitem__,
                          'testuser1@rt-')

    def test_set_item(self):
        tmpUser = User("Test", "test@rt-rk.com", "5.5.5.5", ["test"])
        self.register['test@rt-rk.com'] = tmpUser
        self.assertEqual(self.register['test@rt-rk.com'], tmpUser)

    def test_set_item_instance_of_user_exception(self):
        self.assertRaises(Exception, self.register.__setitem__,
                          'test@rt-rk.com', 1)

    def test_get_user_name(self):
        self.assertEqual(self.register['testuser1@rt-rk.com'].name,
                         self.data[0]['name'])

    def test_get_user_ip(self):
        self.assertEqual(self.register['testuser1@rt-rk.com'].ip,
                         self.data[0]['ip'])

    def test_get_user_devices(self):
        self.assertEqual(self.register['testuser1@rt-rk.com'].devices,
                         self.data[0]['devices'])

    def test_is_valid_ip(self):
        self.assertEqual(True, self.register._is_valid_ip("12.12.12.12"))

    def test_is_valid_email(self):
        self.assertEqual(True, self.register._is_valid_email(self.data[0][
                                                              'email']))

    def test_set_new_devices(self):

        listDevicesTmp = User("Test", "testuser1@rt-rk.com", "1.1.1.1",
                              ["test", "test1"])
        result = ["desktop RTRK-03", "mobile MB-546",
                                "watch W-123", "test", "test1"]
        self.register._set_new_devices(listDevicesTmp)
        self.assertEqual(self.register['testuser1@rt-rk.com'].devices, result)

    def test_load_json(self):
        tmpRegister = Register()
        tmpRegister._load_json_file("test.json")
        self.assertEqual(tmpRegister.users, self.register.users)

    def test_set_user_name(self):
        """to do"""


if __name__ == '__main__':
    unittest.main()

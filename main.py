from classes.Register import Register
from sys import argv
from os import path
from classes.User import User

if __name__ == "__main__":
    json_files = set()
    if len(argv) < 2:
        print("Please, specify path to json files as arguments!")
        exit(-1)
    for i in range(1, len(argv)):
        if not path.isfile(argv[i]):
            raise Exception(f"Path to file isn't correct! {argv[i]}")
            exit(-1)
        print(f"Loading JSON objects from: {argv[i]}")
        json_files.add(argv[i])

    register = Register(json_files)
    print("Register is created!")
    print("Registar: ")
    print(register)
    print(f"Register have {len(register)} items!")
    print(f"Data of user1@rt-rk.com: {register['user1@rt-rk.com']}")
    print("Change value of user1@rt-rk.com!")
    register['user1@rt-rk.com'] = User("changeUser", "user1@rt-rk.com",
                                        '8.8.8.8', ['test_device'])
    register['user7@rt-rk.com'] = User("User7", "user7@rt-rk.com",
                                       "7.7.7.7", ["device10"])
    print("Register after change: ")
    print(register)
    print(f"Name of user7@rt-rk.com:"
          f" {register.get_user_name('user7@rt-rk.com')}")
    print(f"Ip of user7@rt-rk.com:"
          f" {register.get_user_ip('user7@rt-rk.com')}")
    print(f"Devices of user7@rt-rk.com:"
          f" {register.get_user_devices('user7@rt-rk.com')}")
    register.set_user_name('user7@rt-rk.com', "new_user")
    print(f"New name of user7@rt-rk.com:"
          f" {register.get_user_name('user7@rt-rk.com')}")
    register.set_user_ip('user7@rt-rk.com', "5.5.5.5")
    print(f"New ip of user7@rt-rk.com:"
          f" {register.get_user_ip('user7@rt-rk.com')}")
    register.set_user_devices('user7@rt-rk.com', list())
    print(f"New devices of user7@rt-rk.com:"
          f" {register.get_user_devices('user7@rt-rk.com')}")

    register2 = Register([r"C:\Users\bpetrusic\Desktop\Projektni "
                          r"Kurs\nit-ce-08-zadatak\json_file_1.json"])
    print("Register 2:")
    print(register2)
    print("Register: ")
    print(register)
    register3 = register + register2
    print("Register 3: ")
    print(register3)
    register4 = register * register2
    print("Register 4: ")
    print(register4)